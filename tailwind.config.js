module.exports = {
  content: ["./*.html"],
  theme: {
    fontFamily: {
      op: ["Overpass", "sans-serif"],
      ub: ["Ubuntu", "sans-serif"],
    },
    fontWeight: {
      light: 300,
      normal: 400,
      medium: 500,
      semibold: 600,
      bold: 700,
    },
    extend: {
      fontSize: {
        "3.5xl": "2rem",
      },
      colors: {
        "l-red": "hsl(356, 100%, 66%)",
        "vl-red": "hsl(355, 100%, 74%)",
        "vd-blue": "hsl(208, 49%, 24%)",
        "g-blue": "hsl(240, 2%, 79%)",
        "vdg-blue": "hsl(207, 13%, 34%)",
        "vdb-blue": "hsl(240, 10%, 16%)",
        "grad-vlr": "hsl(13, 100%, 72%)",
        "grad-lr": "hsl(353, 100%, 62%)",
        "grad-vdgb": "hsl(237, 17%, 21%)",
        "grad-vddb": "hsl(237, 23%, 32%)",
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
